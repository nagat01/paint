﻿[<AutoOpen>]
module FStap.Bitmap
open System open System.Windows open System.Windows.Controls
open System.Windows.Media
open System.Windows.Media.Imaging


type ArrayBitmap(arr : i[], w) =
  let idx (p:Po) = p.ix + p.iy * w
  let h = arr.len / w

  member __.arr = arr

  member __.paste (arr':i[]) =  
    Array.Copy(arr', arr, arr.len)

  member __.Item 
    with get i   = Co.ofI arr.[i] 
    and  set i v = arr.[i] <- Co.toI v

  member __.Item 
    with get p   = __.[idx p]
    and  set p v = __.[idx p] <- v

  member __.Item 
    with get (x, y)   = __.[x + y * w] 
    and  set (x, y) v = __.[x + y * w] <- v

  member __.Item 
    with get (x, y)   = __.[int(x:f) + int(y:f) * w] 
    and  set (x, y) v = __.[int(x:f) + int(y:f) * w] <- v

  member __.tryGet (p:Po) = 
    if isWithinLess 0 w p.X.i && isWithinLess 0 h p.Y.i 
    then Some __.[p] 
    else None


  // bilinear
  member __.bilinear (p:Po) = 
    let i  = idx p
    let c0 = arr.[i]
    let c1 = arr.[i+1]
    let c2 = arr.[i+w]
    let c3 = arr.[i+w+1]
    let x1 = p.X.frac * 255. |> int
    let y1 = p.Y.frac * 255. |> int
    let x0 = 256 - x1
    let y0 = 256 - y1
    let w0 = x0 * y0 
    let w1 = x1 * y0 
    let w2 = x0 * y1 
    let w3 = x1 * y1
    let a = (iToA c0 * w0 + iToA c1 * w1 + iToA c2 * w2 + iToA c3 * w3)
    let r = (iToR c0 * w0 + iToR c1 * w1 + iToR c2 * w2 + iToR c3 * w3)
    let g = (iToG c0 * w0 + iToG c1 * w1 + iToG c2 * w2 + iToG c3 * w3)
    let b = (iToB c0 * w0 + iToB c1 * w1 + iToB c2 * w2 + iToB c3 * w3)
     
    ((a <<< 8 ) &&& 0xff000000) ||| 
    ((r       ) &&& 0x00ff0000) |||
    ((g >>> 8 ) &&& 0x0000ff00) |||
    ((b >>> 16) &&& 0x000000ff)


  // neighbor
  member __.neighborsAverage rad (p:Po) = 
    let xp, yp = p.X.i, p.Y.i
    let mutable asum, rsum, gsum, bsum = 0, 0, 0, 0
    let rad2 = rad * rad
    for y in max 0 (yp-rad) .. min (yp+rad) (h-1) do
      let yd2 = pown (y-yp) 2
      let xd  = rad2 - yd2 |> float |> sqrt |> int
      let offset = y * w
      for x in max 0 (xp-xd) .. min (xp+xd) (w-1) do
        let co = arr.[x + offset]
        asum <- asum + iToA co
        rsum <- rsum + iToR co
        gsum <- gsum + iToG co
        bsum <- bsum + iToB co
    if asum = 0 || (isWithinLess 0 w xp && isWithinLess 0 h yp).n then
      0
    else
      let a = __.[xp, yp].a
      Co.argbToI a (rsum * a / asum) (gsum * a / asum) (bsum * a / asum)
      
  // line
  member __.line (len:f) (dir:Vec) (orig:Po)  = seq {
    dir.Normalize()
    for m in -len .. len do
      let p = orig + (dir *. m)
      let x' = p.ix
      let y' = p.iy
      yield __.[x', y'] }


  // ctors
  new (w, h)  = ArrayBitmap(arrayZero (w*h), w)
  new (sz:Sz) = ArrayBitmap(sz.iw, sz.ih)

