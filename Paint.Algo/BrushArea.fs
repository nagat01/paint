﻿[<AutoOpen>]
module FStap.BrushArea
open System.Collections.Generic


let inline check (buffer:i[]) w (masks:b[][]) (xmis:i[]) (xmas:i[]) ymi yma (queue:Queue<i*i>) c0 x y =
  if ymi <= y && y <= yma then
    let iy = y - ymi
    let ix = x - xmis.[iy]
    if xmis.[iy] <= x && x <= xmas.[iy] && masks.[iy].[ix].n then
      let c1 = buffer.[x + y * w]
      if isSmallDiff 0xe0e0e0e0 c0 c1 then
        queue.Enqueue(x,y)
      masks.[iy].[ix] <- true


// blush
type Paint with
  member __.linesBrushArea (isLeak:b) (buffer:i[]) (rad:f) x0 y0 x1 y1 =
    let rad  = rad + 1.
    let radi = rad.ceili
    let w   = __.iw
    let xe  = __.xend
    let ye  = __.yend
    let xmi = within 0 xe (mini x0 x1 - radi)
    let xma = within 0 xe (maxi x0 x1 + radi)
    let ymi = within 0 ye (mini y0 y1 - radi)
    let yma = within 0 ye (maxi y0 y1 + radi)
    let dx  = dist x0 x1
    let dy  = dist y0 y1
    let len = distXY x0 y0 x1 y1 
    let half = if dy = 0. then rad + dx / 2. else rad * len / dy 

    let xOfY y = coordX'' x0 y0 x1 y1 (float y)
    let yOfX x = coordY'' x0 y0 x1 y1 (float x)

    let lineOfY y =
      let xc = xOfY (y-1)
      let x0 = max (xc - half).i xmi 
      let wl = min (half * 2.).ceili (min (xma + radi) w - x0)
      r32 x0 y wl 1, xc.i

    let lenY  = yma - ymi + 1
    let lines = arrayZero lenY
    let xmis  = arrayZero lenY
    let xmas  = arrayZero lenY
    let masks = arrayZero lenY
    let queue = Queue()
    for y in ymi .. yma do
      let i = y - ymi
      let line, xc = lineOfY y
      lines.[i] <- line
      if line.w > 0 then
        masks.[i] <- Array.create (line.w+1) isLeak
        if isLeak.n then
          xmis.[i] <- max 0  line.X
          xmas.[i] <- min ye line.r
          let ymi = max 0  (mini y0 y1)
          let yma = min ye (maxi y0 y1)
          if 0 <= xc && xc < w && ymi <= y && y <= yma then
            queue.Enqueue(xc, y)

    for x in max 0 (mini x0 x1) .. min xe (maxi x0 x1) do
      let y = (yOfX x).i
      if 0 <= x && x < w && ymi <= y && y <= yma then
        queue.Enqueue(x, y)

    if isLeak.n then
      let x = within 0 xe ((x0 + x1).i / 2)
      let y = within 0 ye ((y0 + y1).i / 2)
      let c0 = buffer.[x + y * w]
      while queue.Count > 0 do
        let x, y = queue.Dequeue()
        check buffer w masks xmis xmas ymi yma queue c0 (x-1) y 
        check buffer w masks xmis xmas ymi yma queue c0 (x+1) y 
        check buffer w masks xmis xmas ymi yma queue c0 x (y-1) 
        check buffer w masks xmis xmas ymi yma queue c0 x (y+1) 
    
    Seq.zip lines masks |> Seq.filter(fun(line,_) -> line.HasArea)