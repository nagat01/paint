﻿[<AutoOpen>]
module FStap.Bucket
open System.Collections.Generic


type Paint with
  member __.bucket (wbs:Wb []) co (margin:f) (p:Po) =
    // consts
    let co = Co.toI co
    let w  = __.iw
    let h  = __.ih
    let xe = __.xend
    let ye = __.yend
    let ip = p.ix + p.iy * w

    // bitmask
    let bitMask bit = Seq.sum [ for i in 0..3 do for j in bit .. 7 -> 1 <<< (j + i * 8) ]
    let small = log margin / log 2. |> int
    let big   = small + 1
    let small = bitMask small
    let big   = bitMask big 

    let margin = margin.i

    // bitmap
    let bmp  = __.wb.pixels
    let bmp0 = wbs.[0].pixels
    let bmp1 = wbs.[1].pixels
    let c0   = bmp0.[ip]
    let c1   = bmp1.[ip]
    let similar i = 
      let c0' = bmp0.[i]
      let c1' = bmp1.[i]
      if isSmallDiff small c0 c0' && isSmallDiff small c1 c1' then
        true
      elif isBigDiff big c0 c0' || isBigDiff big c1 c1' then 
        false
      else
        isSimilar'' margin c0 c0' && 
        isSimilar'' margin c1 c1'

    // search
    let notyet  = Array.create (w * h) true
    let movable = Array.create (w * h) true
    for offset, x in cart2 [0; w * ye ] [0 .. xe] do
      movable.[x + offset] <- false
    for offset, x in cart2 [0; xe] [0 .. w .. w * ye] do
      movable.[x + offset] <- false
    let queue = Queue()
    if movable.[ip] then
      queue.Enqueue ip
      while queue.Count > 0 do
        let i = queue.Dequeue()
        bmp.[i] <- co
        if similar i && movable.[i] then
          if notyet.[i-1] then notyet.[i-1] <- false; queue.Enqueue (i-1)
          if notyet.[i+1] then notyet.[i+1] <- false; queue.Enqueue (i+1)
          if notyet.[i-w] then notyet.[i-w] <- false; queue.Enqueue (i-w)
          if notyet.[i+w] then notyet.[i+w] <- false; queue.Enqueue (i+w)
      __.wb.writeRect __.wb.r32 bmp
      __.setBuffer