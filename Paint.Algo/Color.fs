﻿[<AutoOpen>]
module FStap.Color
open System open System.Windows open System.Windows.Controls


// hsv
let ofHsv'' h s v =
  let f = h &&& 255
  let p = (v * (255 - s)              ) >>> 8
  let q = (v * (65025 - f * s)        ) >>> 16
  let t = (v * (65025 - (255 - f) * s)) >>> 16
  match h >>> 8 with
  | 1 -> q, v, p
  | 2 -> p, v, t
  | 3 -> p, q, v
  | 4 -> t, p, v
  | 5 -> v, p, q
  | _ -> v, t, p

let toHsv'' r g b =
  let M = max3 r g b
  let m = min3 r g b
  let c = M - m
  if c = 0 then
    0, 0, M
  else
    let h = 
      if   M = r then (g-b) * 255 / c + 1536
      elif M = g then (b-r) * 255 / c + 512
      else            (r-g) * 255 / c + 1024
    let s = if c = 0 then 0 else c * 255 / M
    h, s, M


// similarity
let inline isBigDiff   mask c0 c1 =
  (c0 ^^^ c1) &&& mask <> 0

let inline isSmallDiff mask c0 c1 =
  (c0 ^^^ c1) &&& mask = 0

let inline isSimilar'' margin c0 c1 = 
  let a0 = iToA c0 
  let r0 = iToR c0 
  let g0 = iToG c0 
  let b0 = iToB c0
  let a1 = iToA c1 
  let r1 = iToR c1 
  let g1 = iToG c1 
  let b1 = iToB c1 
  max4
    (if a0 > a1 then a0 - a1 else a1 - a0)
    (if r0 > r1 then r0 - r1 else r1 - r0)
    (if g0 > g1 then g0 - g1 else g1 - g0)
    (if b0 > b1 then b0 - b1 else b1 - b0)
  < margin