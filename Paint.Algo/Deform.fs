﻿[<AutoOpen>]
module FStap.Deform
open System open System.Windows open System.Windows.Controls  
open System.Runtime.InteropServices


type Paint with
  member __.restoreTris (trips:Triangle seq) =
    let lineDest = __.lineDest
    lineDest.clear
    for trip in trips do
      __.wb.Lock()
      for line in trip.lines |%> __.r32.inside >%? R32.hasArea do
        let offset = (line.Y * __.iw + line.X) * 4
        let ptr = __.wb.BackBuffer + IntPtr offset
        Marshal.Copy(lineDest, 0, ptr, line.w)
        __.wb.AddDirtyRect line
      __.wb.Unlock()

  member __.transTrs (trios:Triangle seq) (trips:Triangle seq) = 
    let lineDest = __.lineDest
    for otri, ptri in Seq.zip trios trips do
      let x0, y0, x1, y1, x2, y2 = otri.xy3
      let xo, yo, xa, ya, xb, yb = ptri.xy3
      let x1 = x1 - x0
      let x2 = x2 - x0
      let y1 = y1 - y0
      let y2 = y2 - y0
      let xa = xa - xo
      let ya = ya - yo
      let xb = xb - xo
      let yb = yb - yo
      __.wb.Lock()
      for line in ptri.lines do
        let line = __.r32.inside line
        if R32.hasArea line then
          let y = float line.Y - yo
          let x = float line.X - xo
          for i in 0 .. line.w - 1 do
            let x = x + float i
            let r0, r1 = coord'' xa ya xb yb x y
            let p = 
              po( x0 + r0 * x1 + r1 * x2 )
                ( y0 + r0 * y1 + r1 * y2 )
            lineDest.[i] <- __.buffer.bilinear p
          let offset = (line.Y * __.iw + line.X) * 4
          let ptr = __.wb.BackBuffer + IntPtr offset
          Marshal.Copy(lineDest, 0, ptr, line.w)
          __.wb.AddDirtyRect line
      __.wb.Unlock()


  member __.deform isTrans (os:Po seq) (ps:Po seq) (cs:Po seq) =
    match os.list, ps.list, cs.list with
    | os, ps, cs when os.len = 4 ->
      let tris2 = function
        | [a; b; c; d] -> [ Triangle(a,b,c); Triangle(a,c,d) ]
        | _ -> []
      let rec tris n (ps:Po list) = [
        let ms = [ for i in 0 .. 3 -> ps.[i].mid ps.[(i + 1) % 4] ]
        let p = intersection ms.[0] ms.[2] ms.[1] ms.[3]
        for i in 0 .. 3 do
          let a = ms.[(i - 1) %^ 4]
          let b = ps.[i]
          let c = ms.[i]
          if n > 0 then
            yield! tris (n-1) [a; b; c; p]
          yield Triangle(b, a, p)
          yield Triangle(b, c, p) ]
      if isTrans then
        __.transTrs (tris 1 os) (tris 1 cs)
      else
        __.restoreTris (tris2 ps)
       
    | [o0; o1; o2], [p0; p1; p2], [c0; c1; c2] ->
      let trio = [Triangle(o0, o1, o2)]
      let trip = [Triangle(p0, p1, p2)]
      let tric = [Triangle(c0, c1, c2)]
      if isTrans then
        __.transTrs trio tric
      else
        __.restoreTris trip

    | _ -> ()