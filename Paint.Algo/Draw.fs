﻿[<AutoOpen>]
module FStap.Draw
open System open System.Windows open System.Windows.Controls  
open System.Runtime.InteropServices

type Paint with
  member __.draw isLeak (buffer:i[]) tool co opa rad0 rad1 (p0:Po) (p1:Po) = 
    __.wb.Lock()
    let arrBuffer = __.buffer.arr
    let mask = __.mask
    let coI = Co.toI co
    let coA, coR, coG, coB = co.argb
    let half = max rad0 rad1
    let mgn  = max 1. half
    let x0, y0 = p0.X, p0.Y
    let x1, y1 = p1.X, p1.Y
    let x01    = x1 - x0
    let y01    = y1 - y0
    let v2  = x01 * x01 + y01 * y01
    let ptr = __.wb.BackBuffer
    for rLine, fill in __.linesBrushArea isLeak buffer mgn x0 y0 x1 y1 do
      let lineSrc = __.lineSrc
      let len = rLine.w
      let offset = __.iw * rLine.Y + rLine.X
      let ptr = ptr + IntPtr (offset * 4)
      Marshal.Copy(ptr, lineSrc, 0, len)
      for x in 0 .. len - 1 do
        let i = offset + x
        let x2  = x + rLine.X |> float
        let y2  = rLine.Y |> float
        let x02 = x2 - x0
        let y02 = y2 - y0
        let x12 = x2 - x1
        let y12 = y2 - y1
        let d   = 
          if x01 = 0. && y01 = 0. then
            x02 * x02 + y02 * y02 |> sqrt
          else
            let u = (x01 * x02 + y01 * y02) / v2 |> within 0. 1.
            let dx = x2 - (x0 + u * x01)
            let dy = y2 - (y0 + u * y01)
            dx * dx + dy * dy |> sqrt
        let l0  = x02 * x02 + y02 * y02 |> sqrt
        let l1  = x12 * x12 + y12 * y12 |> sqrt
        let rad = (rad0 * l1 + rad1 * l0) / (l0 + l1)
        let mul = min 1. rad ** 2.
        let a   = within 0. 1. (rad - d) * mul * opa
        let ay = 255. * a |> int
        let aSwap = 255. - 255. * a |> int
        let aSub = max 0 (coA - ay)

        let inline density mul c0 =
          let a0 = ((c0 >>> 24) &&& 255) * mul
          let r0 = ((c0 >>> 16) &&& 255) * mul 
          let g0 = ((c0 >>>  8) &&& 255) * mul
          let b0 = ( c0         &&& 255) * mul
          ((a0 <<< 16) &&& 0xff000000) ||| 
          ((r0 <<<  8) &&& 0x00ff0000) |||
          ((g0       ) &&& 0x0000ff00) |||
          ((b0 >>>  8) &&& 0x000000ff)

        let inline blend c0 =
          let a0 = (c0 >>> 24) &&& 255 
          let r0 = (c0 >>> 16) &&& 255 
          let g0 = (c0 >>>  8) &&& 255 
          let b0 =  c0         &&& 255 
          let a1 = coA * ay
          let inline blend v0 v1 = (v0 * (65535 - a1) + v1 * 255 * ay) >>> 16
          lineSrc.[x] <- 
            (blend a0 coA <<< 24) ||| 
            (blend r0 coR <<< 16) ||| 
            (blend g0 coG <<<  8) ||| 
            (blend b0 coB)
 
        if a > mask.[i] && fill.[x] then
          mask.[i] <- a
          match tool with
          | DrawMode.Pen ->
            blend arrBuffer.[i]
          | DrawMode.Marker ->
            lineSrc.[x] <- coI
          | DrawMode.Blur -> 
            (po x2 y2).footLineSegment p0 p1 
            |> __.buffer.neighborsAverage rad.ceili 
            |> blend
          | DrawMode.Eraser -> 
            lineSrc.[x] <- density aSwap arrBuffer.[i]             
          | DrawMode.EraserSubtract -> 
            lineSrc.[x] <- density aSub arrBuffer.[i]
      Marshal.Copy(__.lineSrc, 0, ptr, len)
      __.wb.AddDirtyRect rLine
    __.wb.Unlock()
