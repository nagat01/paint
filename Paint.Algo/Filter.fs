﻿[<AutoOpen>]
module FStap.Filter
open System open System.Windows open System.Windows.Controls  


type Paint with
  member __.vivid n =
    let m = 255 - n
    let pxs = __.wb.pixels
    for i in 0.. pxs.ilast do
      let co = pxs.[i]
      let a  = iToA co
      let h, s, v = toHsv'' (iToR co) (iToG co) (iToB co)
      let v' = min (v * 255 / m) 255
      let s' = min (s + (v' - v) * 2) 255
      let r, g, b = ofHsv'' h s' v'
      pxs.[i] <- argbToI a r g b
    __.wb.writeAll pxs

  member __.highContrast n =
    let table = Array.init 256 (fun i -> within 0 255 (i - n + i * n * 2 / 255))
    let pxs = __.wb.pixels
    for i in 0.. pxs.ilast do
      let co = pxs.[i]
      let a  =         iToA co
      let r  = table.[ iToR co ]
      let g  = table.[ iToG co ]
      let b  = table.[ iToB co ]
      pxs.[i] <- argbToI a r g b
    __.wb.writeAll pxs

  member __.cleanUp =
    let w    = __.iw
    let h    = __.ih
    let pxs  = __.wb.pixels
    let res  = arrayZero pxs.len
    let line = arrayZero w
    for y in 1 .. h - 2 do
      let o = (y - 1) * w
      for x in 0 .. w - 1 do 
        let o = x + o
        line.[x] <- max3 (iToA pxs.[o]) (iToA pxs.[o + w]) (iToA pxs.[o + w * 2])
      let o = y * w
      for x in 1 .. w - 2 do
        let ma = max3 line.[x - 1] line.[x] line.[x + 1]
        res.[o + x] <- if ma > 127 then pxs.[o + x] else 0
    __.wb.writeAll res        
      
