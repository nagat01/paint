﻿[<AutoOpen>]
module FStap.Math
open System open System.Windows open System.Windows.Controls

let inline distXY x0 y0 x1 y1 = 
  sqr (x0 - x1) + sqr (y0 - y1) |> sqrt

// coordinate
let inline coord'' xa ya xb yb xc yc =
  let d0 = xa * yb - ya * xb
  let d1 = xb * ya - yb * xa
  ( if   d0 <> 0. then (xc * yb - yc * xb) / d0
    elif ya <> 0. then yc / ya
    else 0. ) ,
  ( if   d1 <> 0. then (xc * ya - yc * xa) / d1
    elif yb <> 0. then yc / yb
    else 0. )

let inline coordX'' x0 y0 x1 y1 y =
  if y1 - y0 = 0. 
  then (x0 + x1) / 2.
  else x0 + (x1 - x0) * (y - y0) / (y1 - y0)

let inline coordY'' x0 y0 x1 y1 x =
  if x1 - x0 = 0. 
  then (y0 + y1) / 2.
  else y0 + (y1 - y0) * (x - x0) / (x1 - x0)



// triangle
type Triangle (p0:Po, p1:Po, p2:Po) = 
  let _ps  = [|p0; p1; p2|]
  let x0 = p0.X
  let y0 = p0.Y
  let x1 = p1.X
  let y1 = p1.Y
  let x2 = p2.X
  let y2 = p2.Y

  let pt   = Seq.minBy poY _ps
  let pb   = Seq.maxBy poY _ps
  let xt   = pt.X
  let yt   = pt.Y
  let xb   = pb.X
  let yb   = pb.Y

  let psnt = _ps.filter((<>)pt)
  let psnb = _ps.filter((<>)pb)
  let xnt0 = psnt.[0].X
  let ynt0 = psnt.[0].Y
  let xnt1 = psnt.[1].X
  let ynt1 = psnt.[1].Y
  let xnb0 = psnb.[0].X
  let ynb0 = psnb.[0].Y
  let xnb1 = psnb.[1].X
  let ynb1 = psnb.[1].Y

  member __.po3 = p0, p1, p2
  member __.xy3 = x0, y0, x1, y1, x2, y2
  member __.ps  = _ps 
  member __.r32 = R32.ofPos _ps
  member __.t   = yt
  member __.b   = yb
  member __.isInTriangle (p:Po) = p.isInTriangle  p0 p1 p2
  member __.coord (p:Po) = p.coord p0 p1 p2

  member __.xRange y =
    let t0 = coordX'' xt yt xnt0 ynt0 y
    let t1 = coordX'' xt yt xnt1 ynt1 y
    let b0 = coordX'' xb yb xnb0 ynb0 y
    let b1 = coordX'' xb yb xnb1 ynb1 y
    max (min t0 t1) (min b0 b1) ,
    min (max t0 t1) (max b0 b1)
    
  member __.lines = seq {
    for y in __.r32.ys -> 
      let mi, ma = __.xRange (float y)
      r32 mi.i y (ma - mi).ceili 1 }


type Point with
  member __.translate (s:Triangle) (d:Triangle) =
    let s0, s1, s2 = s.po3
    let r0, r1 = d.coord __
    s0 + r0 * (s1 - s0) + r1 * (s2 - s0)

