﻿[<AutoOpen>]
module FStap.Information
open System open System.Windows open System.Windows.Controls  


type Paint with
  member __.histogramHsv =
    let table  = Array.init 256  (fun i -> i * 50    / 255)
    let tableH = Array.init 3073 (fun i -> i % 46080 / 921)
    let hs = arrayZero 51
    let ss = arrayZero 51
    let vs = arrayZero 51
    for i in __.wb.pixels do
      let h, s, v = toHsv'' (iToR i) (iToG i) (iToB i)
      let h = tableH.[h]
      let s = table .[s]
      let v = table .[v]
      hs.[h] <- hs.[h] + 1
      ss.[s] <- ss.[s] + 1
      vs.[v] <- vs.[v] + 1
    Array.zip3 hs ss vs

  member __.histogramHsvString =
    let hsvss = Seq.chunkBySize 2 __.histogramHsv
    let scale x  = If (x = 0) 0. (log !.x) / log 10.
    let scale xs = 
      [ for x in xs -> scale x |> sf "%.2f" ] +++ "      "
      |> Seq.truncate 2 |> String.concat ", " 
    [ yield "x,       h,               s,                v"
      for x, hsvss in Seq.zip [0. .. 0.04 .. 1.] hsvss -> 
        let hs, ss, vs = List.unzip3 hsvss.list 
        sf "%.2f,  %s,  %s,  %s" x (scale hs) (scale ss) (scale vs) 
    ].concat "\r\n"