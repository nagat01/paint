﻿[<AutoOpen>]
module FStap.Manipulate
open System open System.Windows open System.Windows.Controls  


type Paint with
  member __.transfer isOver (wbS:Wb) (wbD:Wb) =
    let pxs  = __.wb.pixels
    let pxsS = wbS.pixels
    let pxsD = wbD.pixels
    wbS.writeRectIdx __.r32 <| fun i ->
      if Co.iToA pxs.[i] > 0 then 0 else pxsS.[i]
    wbD.writeRectIdx __.r32 <| fun i ->
      if Co.iToA pxs.[i] > 0 then 
        if isOver then pxsS.[i] else blendi pxsD.[i] pxsS.[i]
      else 
        pxsD.[i]

  member __.flipHorizontal =
    let pxs = __.wb.pixels
    let w   = __.iw
    let index i = i + w - 1 - 2 * (i % w)
    pxs
    |> Array.mapi(fun i _ -> pxs.[index i])
    |> __.wb.writeAll

  member __.flipVertical =
    let pxs = __.wb.pixels
    let w   = __.iw
    let h   = __.ih
    let index i = i % w + (h - 1 - i / w) * w
    pxs
    |> Array.mapi(fun i _ -> pxs.[index i])
    |> __.wb.writeAll

  member __.rotate isClockwise = 
    let pxs = __.wb.pixels
    let w   = __.iw
    let h   = __.ih
    let wb  = mkWriteableBitmap h w 
    if isClockwise then
      let index i = (i / h) + (h - 1 - (i % h)) * w
      pxs |> Array.mapi(fun i _ -> pxs.[index i]) |> wb.writeAll
    else
      let index i = (w - 1 - i / h) + (i % h) * w
      pxs |> Array.mapi(fun i _ -> pxs.[index i]) |> wb.writeAll
    __.init wb
