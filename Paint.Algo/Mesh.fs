﻿[<AutoOpen>]
module FStap.Mesh
open FStap open System open System.Windows open System.Windows.Controls  

let sortPoint __ = List.sortBy (fun(Po(x,y))-> (x,y)) __

let inTr margin o a b (__:Po) =                
  let a, b = __.coord o a b
  let within = isWithin (0. + margin) (1. - margin) 
  within a && within b && within (a + b)

let noInsidePoint p0 p1 p2 (__:_ seq) = 
  __.any(fun __ -> inTr 0.01 p0 p1 p2 __).n

let inline _3id np p0 p1 p2 =
  (p0 * np + p1) * np + p2

let inline _4id np p0 p1 p2 p3 =
  ((p0 * np + p1) * np + p2) * np + p3

let inline polyline3id np p0 p1 p2 = 
  min 
    (_3id np p0 p1 p2) 
    (_3id np p2 p1 p0) 

let inline polygon4id np p0 p1 p2 p3 = 
  min 
    (min (_4id np p0 p1 p2 p3) (_4id np p1 p2 p3 p0))
    (min (_4id np p2 p3 p0 p1) (_4id np p3 p0 p1 p2))

let inline triangleid np p0 p1 p2 =
  let p0, p1 = minmax p0 p1
  let p1, p2 = minmax p1 p2
  let p0, p1 = minmax p0 p1
  _3id np p0 p1 p2

type SortedSet<'a> = Collections.Generic.SortedSet<'a>

type Mesh (ps: Po seq) =
  let lls    = nC2 ps.list
  let aps    = ps.array
  let np     = ps.len
  let nl     = lls.len
  let rangep = [0 .. np - 1]
  let rangel = [0 .. nl - 1]
  let lineIndex = Array2D.init np np <| fun p0 p1 ->
    let i0, i1 = minmax p0 p1
    i0 + i1 * (i1 - 1) / 2 

  let linePoints = Array.init nl <| fun index ->
    let index = index * 2
    let mutable i1 = index |> float |> sqrt |> int
    let mutable i0 = (index - i1 * (i1 - 1)) / 2
    if i0 >= i1 then
      i1 <- i1 + 1
      i0 <- (index - i1 * (i1 - 1)) / 2
    i0, i1

  let lineRowPoints index =
    let i0, i1 = linePoints.[index]
    aps.[i0], aps.[i1]

  let notCross = Array2D.init nl nl <| fun i j ->
    let p0, p1 = lineRowPoints i 
    let p2, p3 = lineRowPoints j
    isIntersect 0.001 p0 p1 p2 p3 |> not 

  let isLine (lsAcc:i list) = 
    let table = Array2D.init np np <| fun _ _ -> false
    for l in lsAcc do
      let p0, p1 = linePoints.[l]
      table.[p0, p1] <- true
      table.[p1, p0] <- true
    table

  let getPolygons (lsAcc: i list) =
    let isLine = isLine lsAcc
    let polygons = rarray()

    let polyline3ids = SortedSet()
    let polygon4ids  = SortedSet()
    let triangleids  = SortedSet()

    let mutable value = 0
    let polyline3 = [
      for l0 in lsAcc do 
        let p0, p1 = linePoints.[l0]
        for p2 in rangep do
          if p2 <> p0 && p2 <> p1 then
            let id = polyline3id np p0 p1 p2
            if polyline3ids.Contains(id).n then
              let p0' = aps.[p0]
              let p1' = aps.[p1]
              let p2' = aps.[p2]
              let pOthers = aps.filter ([|p0'; p1'; p2'|].have >> not)
              if noInsidePoint p0' p1' p2' pOthers then
                if isLine.[p2, p0] then
                  polyline3ids.Add id |> ig
                  yield p2, p0, p1
                if isLine.[p2, p1] then
                  polyline3ids.Add id |> ig
                  yield p0, p1, p2 ]
    
    
    let polygon4 = [
      for p0, p1, p2 in polyline3 do
        for p3 in rangep do
          if p3 <> p0 && p3 <> p1 && p3 <> p2 then
            let id = polygon4id np p0 p1 p2 p3
            if polygon4ids.Contains(id).n then
              if isLine.[p3,p0] && isLine.[p3,p2] then
                if isLine.[p0,p2].n && isLine.[p1,p3].n then
                  polygon4ids.Add id |> ig
                  yield p0, p1, p2, p3 ]

    for p0,p1,p2 in polyline3 do
      let id = triangleid np p0 p1 p2
      if triangleids.Contains(id).n then
        if isLine.[p0,p2] then
          triangleids.Add id |> ig
          value <- value + 1
          polygons <+ [p0 ; p1 ; p2]

    for p0,p1,p2,p3 in polygon4 do
        let p0' = aps.[p0]
        let p1' = aps.[p1]
        let p2' = aps.[p2]
        let p3' = aps.[p3]
        let ap4 = [|p0'; p1'; p2'; p3'|]
        if isConvex ap4 then
          let pOthers = aps.filter(ap4.have >> not)
          if noInsidePoint p0' p1' p2' pOthers && noInsidePoint p0' p3' p2' pOthers then
            value <- value + 10
            polygons <+ [p0; p1; p2; p3]
    polygons, value
    
  
  member __.mesh =
    let rec f (remain:i list) (acc:i list) =
      match remain with
      | l0 :: remain ->
        let notCross = acc.forall(fun l1 -> notCross.[l0, l1])
        let r0 = f remain acc
        if notCross then
           let r1 = f remain (l0 :: acc)
           if _x r1 > _x r0 then r1 else r0
        else
          r0
      | _ ->
        getPolygons acc
    match np with
    | 0 | 1 -> rarray [], Seq.empty
    | 2     -> rarray [], Seq.singleton (0, 1)
    | _     ->
      let polygons, _ = f rangel []
      let lines = 
        [ for polygon in polygons do yield! polygon +++ polygon.head |> Seq.pairwise ]
        |> Seq.distinctBy(fun (i0, i1) -> lineIndex.[i0, i1]  )
      polygons, lines
