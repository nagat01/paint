﻿[<AutoOpen>]
module FStap.Paint
open System open System.Windows open System.Windows.Controls  


[<RequireQualifiedAccess>] 
type DrawMode = 
  | Pen    
  | Marker 
  | Eraser 
  | EraserSubtract
  | Blur 


type Paint =
  val image : Image
  [<DefaultValue>] val mutable wb       : Wb
  [<DefaultValue>] val mutable mask     : f []
  [<DefaultValue>] val mutable buffer   : ArrayBitmap
  [<DefaultValue>] val mutable bufUndo  : ArrayBitmap
  [<DefaultValue>] val mutable r32      : R32
  [<DefaultValue>] val mutable iw       : i
  [<DefaultValue>] val mutable ih       : i
  [<DefaultValue>] val mutable xend    : i
  [<DefaultValue>] val mutable yend    : i
  [<DefaultValue>] val mutable sz       : Sz
  [<DefaultValue>] val mutable lineDest : i []
  [<DefaultValue>] val mutable lineSrc  : i []

  new(image:Image, wb:Wb) as __ = 
    { image = image } then __.init wb

  member __.init (wb:Wb) =
    __.image    <*- wb
    __.wb       <- wb
    __.r32      <- wb.r32
    __.iw       <- wb.pw
    __.ih       <- wb.ph
    __.xend    <- wb.pw - 1
    __.yend    <- wb.ph - 1
    __.sz       <- wb.r32.sz
    __.mask     <- arrayZero wb.r32.area
    __.buffer   <- ArrayBitmap(wb.pixels, wb.r32.w)
    __.bufUndo  <- ArrayBitmap(wb.pixels, wb.r32.w)
    __.lineSrc  <- arrayZero 3200 
    __.lineDest <- arrayZero 3200


  // methods
  member __.setBuffer = 
    __.mask.clear
    __.bufUndo.paste __.buffer.arr
    __.wb.copyAll __.buffer.arr

  member __.restoreBuffer = 
    __.wb.writeAll __.buffer.arr

  member __.undo =
    __.wb.writeAll __.bufUndo.arr
    __.setBuffer
    

type Image with
  member __.mkPaint (sz:Sz) = Paint(__, mkWriteableBitmap sz.iw sz.ih)
  member __.mkPaint (wb:Wb) = Paint(__, wb)