﻿module Console
open FStap open System open System.Windows open System.Windows.Controls      


// ui
let dp   = DockPanel()
let  tbx = TextBox'() $ dp $ haStretch


// funcs
let focus<'__> =
  tbx.IsEnabled <- true
  tbx.focus
  immediate' {
  do! 1
  tbx <*- tbx.v.Substring(0, max 0 (tbx.v.len - 1))
  tbx.SelectionStart <- tbx.v.len }

let unfocus<'__> =
  tbx.IsEnabled <- false

let commands = 
  Map.ofList [
    "", fun _ -> ()
    "displayHsv",          fun _ -> ColorSelector.displayHsv <*- true
    "displayRgb",          fun _ -> ColorSelector.displayHsv <*- false
    "viewTopRight",        fun _ -> View.isTopRight <*- true
    "viewTopLeft",         fun _ -> View.isTopRight <*- false
    "rotateClockwise",     fun _ -> Layer.rotateRightAngle true
    "rotateAntiClockwise", fun _ -> Layer.rotateRightAngle false

    "defaultAngle", fun _ -> 
      S.defaultAngle <*- trCanvas.angle
    
    "clear defaultAngle", fun _ ->
      S.defaultAngle <*- 0.
      S.angle <*- 0.
    
    "cleanup", fun _ ->
      Layer.cur.paint.cleanUp

    "histogramHsv", fun _ ->
      Layer.cur.paint.histogramHsvString |>* lblConsole

    "filterHighContrast", fun _ ->
      Layer.cur.paint.highContrast 10

    "filterVivid", fun _ ->
      Layer.cur.paint.vivid 10
  ]

let command _ =
  commands.TryFind tbx.v |%| fun f -> f ()
  unfocus 

let cands _ = 
  commands.kv |%> x_

// events
tbx.LostFocus =+ { unfocus }

tbx.autoComplete(cands, command , false)


// button
let btnConsole =
  keyBtn dp "押す: コンソールに入力できる状態にする" <| fun _ ->
    focus

unfocus