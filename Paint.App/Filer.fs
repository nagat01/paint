module Filer
open FStap open System open System.Windows open System.Windows.Controls      
open System.Windows.Controls.Primitives
open System.Windows.Input
open System.Windows.Media
open System.Windows.Media.Imaging
open System.IO


// funcs
let _openTiff file =
  use fs = new FileStream(file, FileMode.Open, FileAccess.Read)
  let dec = TiffBitmapDecoder(Uri file, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default)
  for frame, layer in Seq.zip dec.Frames Layer.all do
    let frame = FormatConvertedBitmap(frame, PixelFormats.Pbgra32, null, 0.)
    layer.wb <- Wb frame
    S.w <*- frame.Width
    S.h <*- frame.Height
  S.file <*- file

let _saveTiff isBuckup file =
  let enc = TiffBitmapEncoder()
  let add visual =
    let rtb = mkRtb cnvLayer.rsz
    rtb.Render visual
    enc.Frames <+ BitmapFrame.Create rtb
  Layer.all |%| add
  Canvas() $ "fff".bg |> add
  try 
  use fs = new FileStream(file, FileMode.Create)
  enc.Save fs
  if not isBuckup then 
    S.file <*- file
  with _ -> 
    if isBuckup then
      msg "./backup.tifの保存に失敗しました。"
    else
      mf "%sとして保存するのに失敗しました。" file 

   
let _savePng _ = do' {
  let! path = saveFile "savePng" "png"
  let rtb = mkRtb cnvCanvas.rsz
  let! dc = rtb.render 
  dc.DrawRectangle("fff".br, null, Rect cnvCanvas.rsz)
  Layer.all |%| rtb.Render 
  let enc = PngBitmapEncoder()
  enc.Frames <+ BitmapFrame.Create rtb
  use fs = new FileStream(path, FileMode.Create)
  enc.Save fs }


//
let lastDialogOpen = Stopwatch'()
let check<'__>     = lastDialogOpen.isMSec 100 
let restart<'__>   = lastDialogOpen.restart

let mutable isMenu = false


// buttons
let newFile = keyIco Ico.newFile "新規作成" "今あるキャンバスを消去して新しいキャンバスを作成します"
newFile.down =+ {
  do! msgq "新規作成" "すると編集中のファイルが消去されますがよろしいですか？" 
  Layer.ini 
  S.state <*- State.DrawTool
  S.file  <*- ""
  initCanvasPos }


let btnOpenTiff = keyIco Ico.openFile "Tif開く" "Tifファイルを開きます" 
btnOpenTiff.down =+ {
  let! file = openFile "openTif" "tif" 
  if 
    S.file <>. file || 
    msgq "同名のファイルを開きます" "これは保存ではなく、開くです。\r\n編集中のデータは消えてしまいますが大丈夫ですか？" 
  then 
    _openTiff file }


let btnSaveTiff = keyIco Ico.saveFile "TIF保存" ".tif形式で保存します。"
btnSaveTiff.down =+ {
  do! check
  if IsSpace then 
    S.file |*> _saveTiff false 
  else
    saveFile "saveTif" "tif" (_saveTiff false)
  restart }


let btnOpenBackup = keyIco Ico.backup "backupを開く" "backup.tif ファイルを開きます。"
btnOpenBackup.down =+ {
  do! msgq "バックアップを開きます" "編集中のデータは消去されます。本当にバックアップを開きますか？"
  _openTiff (pwd +/ "backup.tif") }


let savePng = keyIco Ico.save "PNG保存" ".png形式で保存します。"
savePng.down =+ {
  do! check 
  _savePng()
  restart }


let menus = [ 
  Key.D1, newFile
  Key.D2, btnOpenTiff
  Key.D3, btnSaveTiff
  Key.D4, btnOpenBackup
  Key.D5, savePng ]


// event
menus |%| fun (_, kpnl) -> 
  kpnl.down =+ { 
    KeyUI.hide menus 
    kpnl |> "fff".bg }


mkTimer 10000 =+! { 
  do! 30.
  pwd +/ "backup.tif" |> _saveTiff true }

loaded =+ { 
  try
    if S.file.v.fileOk.n then failwith "no-file"
    _openTiff S.file.v
  with _ -> 
    S.file <*- ""
  }


// menu
let menu = keyLbl (HStackPanel Ico.file) "メニューを開閉します"
menu.down =+ { KeyUI.swap menus }

