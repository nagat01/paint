﻿module KeyUI
open FStap open System open System.Windows open System.Windows.Controls      
open System.Windows.Input


// modifiers
let modifiers = [ 
  Key.Tab,       keyLbl (HStackPanel "") ""
  Key.LeftCtrl,  keyLbl (HStackPanel "") "押す: 機能の切り替え"
  Key.LeftShift, keyLbl (HStackPanel "") "押す: 機能の切り替え" ] 

KeyUI.show modifiers


// normals
let normals = [
  Key.Escape, Filer.menu

  Key.D1, Console.btnConsole

  Key.Q, ColorH.btnColorH
  Key.W, ColorSV.btnColorSV
  Key.E, ColorSelector.btnColorSelector
  Key.R, BrushSelector.btnBrushSelector

  Key.A, Bucket.btnBucket
  Key.S, Blur.btnBlur
  Key.D, Eraser.btnEraser
  Key.F, Pen.btnPen
  Key.G, LayerManager.btnManager

  Key.Z, Undo.btnUndo
  Key.X, Pan.btnPan
  Key.C, ZoomRotate.btnZoomRotate
  Key.V, Others.btnTool
  Key.B, View.btnView 
  ]

let tools = [
  Key.S, Select.btnSelect
  Key.D, Deform.btnDeform
  ]

S.state => ( function
  | State.DrawTool -> normals
  | State.Tool     -> tools
  >> KeyUI.show )

S.tool => ( function
  | Tool.Select   -> Select.btns
  | Tool.DeformUI -> Deform.btns 
  >> KeyUI.show )


// spaces
let spaces = [
  Key.D3, Filer.btnSaveTiff
  Key.G,  LayerSize.btnSize
  ]
  
wnd.ktoggle Key.Space => fun isVis -> 
  KeyUI.vis isVis spaces

let ini = wnd.ig