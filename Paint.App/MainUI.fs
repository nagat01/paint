﻿module MainUI
open FStap


dpWindow |> fun __ ->
  KeyUI.kuiTop |> __.top
  KeyUI.kuiBottom |> __.bottom
  gridCanvas $ zIdx -1 |> __.top


gridCanvas |> cs [
  cnvCanvas
  View.rectView
  guide
  lblConsole 
  ]


cnvCanvas |> cs [
  cnvLayer
  Select.image
  Deform.cnv 
  ] 

guide.menter =+ { guide |> If guide.isHaLeft haRight haLeft }
guide.defDisc <- ""

let ini = wnd.ig