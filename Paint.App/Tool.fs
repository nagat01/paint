﻿module Others
open FStap open System open System.Windows open System.Windows.Controls      


// tool
let btnTool = 
  keyBtn (HStackPanel "ツール") "押す: パレットとツールを入れ替えます" <| fun _ -> 
    toggleState State.Tool

S.tool =+ {
  for tool, kpnl in [ Select, Select.btnSelect; DeformUI, Deform.btnDeform] do
    kpnl.pnl |> (If (tool = S.tool.v) "fec" "fff").bg
  }

