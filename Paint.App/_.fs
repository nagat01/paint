module Program
open FStap open System open System.Windows open System.Windows.Controls open System.Reflection
[<product "SakuSakuPaint"; version "0.4.5"; company "ながと"; copyright "Copyright © ながと 2014";
  title "SakuSakuPaint"; description "厚塗りに向いた高速操作できるペイントソフト "; STAThread>] SET_ENG_CULTURE
open System.Windows.Input

Main.ini
KeyUI.ini
MainUI.ini

mkTimer 100 =+ { 
  InputMethod.Current.ImeState <- InputMethodState.Off 
  }

loaded =+ { 
  initCanvasPos 
  }

trace true {
INIT_SHARED_VALUES
wnd.run
}
