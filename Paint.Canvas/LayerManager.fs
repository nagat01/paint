[<AutoOpen>]
module FStap.LayerManager
open System open System.Windows open System.Windows.Controls  


let spManager = HStackPanel()
let  _        = Ico.swapLayer $ spManager
let  spLayers = StackPanel()        $ spManager 
let   lbls    = Array.init 2 (fun _ -> Label'() $ spLayers.addHead $ fsz 10 $ wh 60 15 $ hcaRight)


let btnManager = 
  KeyPanel(
    spManager,
    "押す: レイヤーをスワップ\r\nShift: 不透明度をトグル\r\nCtrl + ←→: 不透明度を変更",
    pushf = ( fun _ ->
      if IsShift then
        Layer.cur.opa <*- If (Layer.cur.opa.v < 0.5) 1. 0.
      elif IsCtrl then
        ()
      else 
        Layer.idx.[1 - _1] ),
    dragf = ( fun v -> 
      if IsCtrl then
        Layer.cur.opa <*- within 0. 1. (Layer.cur.opa +. v.X / 100.) )
    )


// events
Layer.idx => fun __ ->
  "fdd".bg lbls.[__]
  "fff".bg lbls.[1 - __]

for i in 0 .. 1 do
  Layer.all.[i].opa => fun opa -> 
    lbls.[i] <*- sf "%.2f" opa

