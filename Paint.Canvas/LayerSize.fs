﻿module FStap.LayerSize
open System open System.Windows open System.Windows.Controls  


let btnSize = 
  let sp = HStackPanel()
  let mkSli ttl (vol:_ sh) = 
    TitledSlider (ttl, 200., 2400., "000", v = vol.v) 
    $ sp $ w 50 $ twoWay vol
  let sliW = mkSli "幅"   S.w
  let sliH = mkSli "高さ" S.h

  keyDragr sp "↓↑: キャンバスの高さを変更\r\n←→: キャンバスの幅を変更" <| fun v ->
    sliW <*- sliW +. v.X 
    sliH <*- sliH +. v.Y  
