﻿module FStap.Pan
open System open System.Windows open System.Windows.Controls  


// pan
let btnPan = 
  KeyPanel(
    HStackPanel Ico.move,
    "ドラッグ: キャンバスを移動\r\nShift: キャンバスを初期位置へ",
    pushf = ( fun _ -> 
      if IsShift then 
        initCanvasPos ),
    dragf = ( fun v -> 
      if IsShift then
        ()
      else
        v * 2. |> trCanvas.Move ) )

