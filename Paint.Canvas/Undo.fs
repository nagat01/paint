﻿module FStap.Undo
open System open System.Windows open System.Windows.Controls  


// undo
let undo _ = 
  if IsShift then
    for __ in Layer.manips do __.undo
  else
    Layer.cur.paint.undo

let btnUndo = 
  keyBtn (HStackPanel Ico.undo) "１回だけUNDO出来ます" undo
