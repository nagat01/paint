﻿module FStap.View
open System open System.Windows open System.Windows.Controls
open System.Windows.Media
open System.Windows.Shapes

let isTopRight = pshb "isTopRight" false

let ratio = sh 2.5

let rectView = 
  Rectangle( Fill = VisualBrush cnvLayer ) 
  $ stroke 1 "fcc"
  $ haRight $ vaBottom

// events
rectView.menter =+ {
  do! ratio =. 2.5 
  let isTop = rectView.isVaTop
  if isTop 
  then rectView $ haRight |> vaBottom
  else rectView $ isTopRight.v.If haRight haLeft |> vaTop
  cnvLayer |> (If isTop "fff" "cfff").bg
  }

gridCanvas.SizeChanged >< ratio >< S.sz' >< S.angle =+ {
  let scale = min (gridCanvas.ah /. S.h) (gridCanvas.aw /. S.w) /. ratio
  let h = S.h *. scale
  let w = S.w *. scale
  rectView.Width <- w
  rectView.Height <- h 
  rectView.RenderTransform <- RotateTransform(S.angle.v, w / 2., h / 2.)
  }


// button
let btnView = 
  keyBtn (HStackPanel Ico.restore)
    "押す: 全体ビューを最大、もしくは1/2サイズに切り替え\r\nShift: 全体ビューの表示、非表示を切り替えます"
  <| fun _ -> 
    ratio <*-
      match IsShift, ratio.v with
      | true, 1000. -> 2.5
      | true, _     -> 1000.
      | _,    1.    -> 2.5
      | _           -> 1.

