﻿module FStap.ZoomRotate
open System open System.Windows open System.Windows.Controls  


// zoom rotate
let btnZoomRotate = 
  KeyPanel(
    HStackPanel Ico.zoomAndRotate,
    "↑↓: キャンバスを拡大縮小\r\n←→: キャンバスを回転\r\nShift: キャンバスを左右反転\r\nCtrl: キャンバスを上下反転",
    pushf = ( fun _ -> 
      if IsShift then 
        Layer.flipHorizontal
      elif IsCtrl then
        Layer.flipVertical ),
    dragf = ( fun vec ->
      S.angle <*- S.angle +. vec.X 
      -0.01 * vec.Y |> trCanvas.Zoom ) )
