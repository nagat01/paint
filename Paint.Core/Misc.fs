﻿[<AutoOpen>]
module FStap.Misc
open FStap open System open System.Windows open System.Windows.Controls
open System.Windows.Input

let keyToS (key:Key) =
  let s = enumToS key 
  match s with
  | "escape"    -> "ESC" 
  | "tab"       -> "TAB"
  | "leftctrl"  -> "CTRL"
  | "leftshift" -> "SHIFT"
  | _ -> if s.isMat "d." then s.[1..1] else s


let TextBoxFocused<'__> = 
  Keyboard.FocusedElement :? TextBox