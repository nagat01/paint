﻿[<AutoOpen>]
module FStap.Stabilizer
open FStap open System open System.Windows open System.Windows.Controls  


type Stabilizer() =
  let poP   = None'<Po>
  let vecP  = None'<Vec>
  let curvP = None'<f>

  let getStabilizedPoint prev cur vec =
    let prev = poP |? prev
    let vecC = cur - prev
    let vec = 
      match vecP, curvP with
      | Some' vecP, Some' curvP when vecP.Length > 0. ->
        let lenP  = vecP.Length
        let lenC  = vecC.Length
        let curvC = vecP.angle vecC
        let fac   = (0.5 - dist curvC curvP) * 100. |> within 0.1 10. 
        let curvC = (curvC * lenC + curvP * lenP * fac) / (lenC + lenP * fac)
        let fac   = min 2. fac
        let len   = ((lenC ** 0.75) / 2. + lenC * fac) / (1. + fac)
        let vec = (vecP * rotator curvC).normalize * len
        vec
      | _ -> vecC * 0.1
    let curv = vecP |%> vec.angle |? 0.
    let cur  = prev + vec
    poP   <*- cur
    vecP  <*- vec
    curvP <*- curv
    prev, cur

  member __.clear =
    poP.none
    vecP.none
    curvP.none

  member __.next (e:DragEventArg) isStabilize =
    let rad0 = x_ e.Pres |? 1.
    let rad1 = _x e.Pres |? 1. 
    let prev, cur =
      if isStabilize then 
        getStabilizedPoint e.Prev e.Cur e.Vec
      else 
        e.Prev, e.Cur
    rad0, rad1, prev, cur

