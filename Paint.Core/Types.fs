﻿[<AutoOpen>]
module FStap.Types
open FStap open System open System.Windows open System.Windows.Controls


type DrawTool = Pen | Blur | Eraser | Bucket
type Tool     = Select | DeformUI
type State    = DrawTool | Tool

type DrawToolInfo (idx:i) as __ = 
  let all = rarray()
  let shdt  v = sh v $ fun m -> all <+ (fun _ -> m <*- v)
  let shf k v = pshf (sf "%s%d" k idx) v $ fun m -> all <+ (fun _ -> m <*- v)
  let hsv'()  = HsvCo(sf "penColor%d" idx) $ fun x -> all <+ (fun _ -> x.ini)
  let _hsvs = shs [hsv'(); hsv'()]

  member val drawTool   = shdt DrawTool.Pen 
  member val hsvs       = _hsvs
  member val idxCo      = _hsvs.idx' 
  member val radPen     = shf "radPen"     3.  
  member val opaPen     = shf "opaPen"     1.   
  member val radEraser  = shf "radEraser"  10.  
  member val opaEraser  = shf "opaEraser"  1.  
  member val radBlur    = shf "radBlur"    10.   
  member val opaBlur    = shf "opaBlur"    1.
  member val mgnBucket  = shf "mgnBucket"  32.
  member val diffBucket = shf "diffBucket" 0.

  member __.hsv   = _hsvs.x
  member __.co'   = __.hsvs |> toSh 
  member __.co    = __.co'.v
  member __.cos   = _hsvs.vs
  member __.Co co = __.co' <*- co
  member __.coPen = __.co.setA __.opaPen.v

  member __.rad =
    match __.drawTool.v with
    | Pen    -> __.radPen.v
    | Eraser -> __.radEraser.v
    | Blur   -> __.radBlur.v
    | _      -> __.mgnBucket.v

  member __.opa =
    match __.drawTool.v with
    | Pen    -> __.opaPen.v
    | Eraser -> __.opaEraser.v
    | Blur   -> __.opaBlur.v
    | _      -> __.diffBucket.v

  member __.e = 
    __.drawTool >< __.co' >< 
    __.opaPen >< 
    __.radPen >< __.radEraser >< __.radBlur >< 
    __.mgnBucket
  
  member __.ini =
    for f in all do f ()
