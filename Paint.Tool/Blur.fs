﻿module FStap.Blur
open System open System.Windows open System.Windows.Controls


let btnBlur =
  let __ = HStackPanel()
  let  _ = Ico.blur $ __
  let  _ = RadOpaPanel(Layer.radBlur, Layer.opaBlur) $ __
  KeyPanel(
    __,
    "押す: ぼかしツールを選択します。\r\n↑↓: ぼかしの不透明度を変更\r\n←→: ぼかしの太さを変更",
    pushf = changeTool DrawTool.Blur, 
    dragf = fun v -> 
      if changeByDragOk then
        cursorModified.restart
        Layer.radBlur.[_1 + v.X / 2.   >> within 1. 25.]
        Layer.opaBlur.[_1 + v.Y / 100. >> within 0. 1. ] )


btnBlur |> toolSelected DrawTool.Blur