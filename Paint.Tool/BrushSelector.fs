﻿module FStap.BrushSelector
open System open System.Windows open System.Windows.Controls


let btnBrushSelector =
  let idx  = sh 0
  let rads = Array.init 2 (fun _ -> sh 1.)
  let opas = Array.init 2 (fun _ -> sh 1.)
  let sp    = HStackPanel()
  let  bdrs = Array.init 2 ( fun _ -> Border() $ sp $ bdr 2 "fff")
  let  brs  = Array.init 2 ( fun i -> BrushLabel(Layer.co', rads.[i], opas.[i]) $ bdrs.[i] )
  idx => fun idx ->
    for i, brush in brs.seqi do
      ( brush:BrushLabel ).isEnabled <- (i = idx)
    Layer.radPen <*-* rads.[idx] 
    Layer.opaPen <*-* opas.[idx]
    for i, border in bdrs.seqi do
      border |> bdr 2 ((idx = i).If "0f0" "fff")
  Layer.radPen => fun __ -> rads.[idx.v] <*- __
  Layer.opaPen => fun __ -> opas.[idx.v] <*- __
  keyBtn sp "押す: ペン先をスワップします" 
    <| fun _ -> idx.[1 - _1]

