﻿module FStap.Bucket
open System open System.Windows open System.Windows.Controls


let btnBucket = 
  let __ = HStackPanel()
  let  _ = Ico.bucket $ __
  let  _ = RadOpaPanel(Layer.mgnBucket, Layer.diffBucket) $ __
  KeyPanel(
    __,
    "押す: 同じ色の領域を塗りつぶし\r\nShift: 一番上のレイヤーの色が同じ領域を塗りつぶし",
    pushf = changeTool DrawTool.Bucket,
    dragf = ( fun v ->
      if changeByDragOk then
        cursorModified.restart
        Layer.mgnBucket.[_1 + v.X   >> within 1. 100.] )
    )

btnBucket |> toolSelected DrawTool.Bucket