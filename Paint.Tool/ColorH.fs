﻿module FStap.ColorH
open System open System.Windows open System.Windows.Controls
open System.Windows.Shapes

let Hsv<'__> = Layer.hsv


// ui
let ca    = Canvas() $ wh 90 25 $ center
let  img  = Image()     $ ca $ w 90
let   wb  = mkWriteableBitmap 90 25 $ img
let  rect = Rectangle() $ ca $ wh 10 25


// events
Layer.co' =+ {
  let mkCo add = ofHsv ((Hsv.h + add) % 1.) 1. 1.
  rect $ mv (Hsv.h * 90.) 0. $ fill (mkCo 0.) |> stroke 1 (mkCo 0.5) }

wb.writeRectXY (r32 0 0 90 25) <| fun x y -> 
  ofHsv (x.f / 90.) 1. 1. |> toI


// button
let btnColorH = 
  keyDragr ca "←→: 色合いを変更" <| fun v -> 
    cursorModified.restart
    (Hsv.h + v.X / 90.) %^ 1. |> Hsv.H

