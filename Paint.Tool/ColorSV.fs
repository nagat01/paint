﻿module FStap.ColorSV
open System open System.Windows open System.Windows.Controls
open System.Windows.Shapes


let Hsv<'__> = Layer.hsv


// ui
let ca    = Canvas() $ wh 90 25 $ center
let  img  = Image()     $ ca $ w 90
let   wb  = mkWriteableBitmap 90 25  $ img
let  rect = Rectangle() $ ca $ wh 10 10 $ tr2 -5 -5


Layer.co' => fun __ ->
  rect $ mv (Hsv.s * 90.) (Hsv.v' * 25.) $ fill __ |> stroke 1 __.inv
  wb.writeRectXY (r32 0 0 90 25) <| fun x y -> 
    ofHsv Hsv.h (x.f / 90.) (y.f / 25.) |> toI


// button
let btnColorSV = 
  keyDragr ca "↑↓: 明るさを変更\r\n←→: 彩度を変更" <| fun v -> 
    cursorModified.restart
    Hsv.S ( Hsv.s  + v.X / 90. |> within 0. 1. )
    Hsv.V ( Hsv.v' + v.Y / 25. |> within 0. 1. ) 
  