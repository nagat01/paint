﻿module FStap.ColorSelector
open System open System.Windows open System.Windows.Controls
open System.Windows.Media
open System.Windows.Shapes


let displayHsv = pshb "displayHsv" false

type ColorRectangle(_size1, ?_strokeTh) as __ =
  inherit Grid()
  let _storokeTh = _strokeTh |? 0.
  let rect   = Rectangle() $ __ $ size1 _size1 $ strokeTh _storokeTh
  let  sp    = StackPanel()      $ __ $ mgn1 (_storokeTh + 1.) $ haRight $ vaCenter
  let  tblks = [| for _ in 0..2 -> ShadowedLabel(6., _fw=FontWeights.Normal) $ sp $ haRight |]

  member __.co 
    with set (co:Color) =
      let hsvToS x = sf "%.0f" (x * 100.) |> fun x -> If (x = "NaN") "" x
      let h, s, v = co.hsv
      let cos = 
        displayHsv.If
          [| (ofHsv h 1. 1.).str; "000"; "000" |] 
          [| "f00"; "0f0"; "00f" |]
      let ns = 
        displayHsv.If
          ( [| h; s; v |] |%> hsvToS )
          [| co.r.s; co.g.s; co.b.s |]
      for i in 0..2 do
        tblks.[i].co <- cos.[i]
        tblks.[i] <*- ns.[i]
      rect |> fill co

  member __.tryCo = try_ { yield rect.Fill.co }

  member __.selected 
    with set b = 
      rect |> strokeCo (If b "0f0" "eee")


type ColorSelector (_idxCo:i sh, _co:Color sh, _getColors) as __ =
  inherit HStackPanel()
  let gridSpoit  = Grid()             $ __ $ mgn1 2
  let  rectColor = ColorRectangle(21) $ gridSpoit 
  let rectsColor = [| for co in "000 fff".words -> ColorRectangle(25, 2.) $ __ |]
  do
    wnd.mmove =+ { rectColor.co <- getColorOnMouse }

    _idxCo => fun idx ->
      _co <*-? rectsColor.[idx].tryCo
      for i, r in rectsColor.seqi do
        r.selected <- (idx = i)

    _co => fun co ->
      for r, co in Seq.zip rectsColor (_getColors()) do
        r.co <- co

  member __.selNext = _idxCo.[fun i -> (i + 1) % 2]


let btnColorSelector = 
  let __ = ColorSelector(Layer.idxCo, Layer.co', (fun _ -> Layer.cos))
  keyBtn __ "押す: 色をスポイト\r\nShift: 選択色の切り替え" <| fun _ -> 
    if IsShift then 
      __.selNext
    else 
      Layer.spoit 

