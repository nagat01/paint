﻿[<AutoOpen>]
module FStap.Deform
open System open System.Windows open System.Windows.Controls  
open System.Windows.Input
open System.Windows.Shapes

type Deform (cnv:Canvas, points: Po list) =
  let mutable alive = true
  let pointsOrig = points.array
  let points     = [| for p in points -> sh p |]
  let polygons, lines = Mesh(pointsOrig).mesh
  let getCur polygon = [for p in polygon -> points.[p].v ]
  let prevPolygons = Dic [ for polygon in polygons -> polygon, getCur polygon ]
  let drag = cnv.lDragOn cnv -? { V (alive && IsShift) }
  let deform () =
    let deform isTrans =
      for polygon in polygons do
        let orig = [for p in polygon -> pointsOrig.[p]]
        let cur = getCur polygon
        let prev = prevPolygons.[polygon]
        Layer.deform isTrans orig prev cur
        if isTrans then
          prevPolygons.[polygon] <- cur
    deform false
    deform true
 
  do
    drag => fun e ->
      if IsShift && e.Vec.Length < 100. then
        for p in points do
          p.quiet (p.v + e.Vec)
        deform ()

    for p in points do
      let elp = Ellipse() $ cnv $ stroke 2 "f70" $ fill "7f70" $ tr2 -10 -10 $ size1 20
      p >=< drag -% { V p.v } => elp.Po
      elp.lDragOn cnv => fun e ->
        p <*- e.Cur
   
    for p0, p1 in lines do
      let line = Line() $ cnv $ stroke 2 "f70" $ nohit
      points.[p0] >=< drag -% { V points.[p0].v } => line.set_p1 
      points.[p1] >=< drag -% { V points.[p1].v } => line.set_p2    

    for p in points do
      p.ini
      p =+ { deform () }

  member __.Delete = alive <- false

  member __.Points = [ for p in points -> p.v ]


// view
let cnv            = Canvas() $ "0fff".bg
let mutable deform = Some(Deform(cnv, []))

let delete () =
  deform |%| fun __ -> __.Delete
  deform <- None

// funcs
let clearView<'__> =
  delete()
  remChildren<Ellipse> cnv
  remChildren<Line> cnv

let write<'__> =
  clearView
  delete()
  setBuffer

let restore<'__> =
  clearView
  delete()
  restoreBuffer


// event
S.sz' => cnv.Sz

S.state >< S.tool =+ {
  cnv.IsHitTestVisible <- IsDeform
  do! IsDeform.n 
  write }

cnv.lclick => fun e -> 
  match deform with
  | Some __ ->
    if __.Points.len < 5 then
      write
      deform <- Some(Deform(cnv, e.pos :: __.Points))
  | None ->
    clearView
    deform <- Some(Deform(cnv, [e.pos]))
 

// buttons
let btnDeform = keyIco Ico.deform (Lbl "変形") "押す: 自由変形ツールを選択します"
btnDeform.down =+ { 
  S.tool.force Tool.DeformUI }

let btnWrite  = keyLbl (HStackPanel "自由変形書き込み") "押す: 自由変形の結果を描き込みます"
btnWrite.down =+ { 
  do! IsDeform 
  write }

let btnClear  = keyLbl (HStackPanel "自由変形クリア") "押す: 自由変形をクリアします"
btnClear.down =+ { 
  do! IsDeform 
  restore }


let btns = [
  Key.W, btnClear
  Key.E, btnWrite ]