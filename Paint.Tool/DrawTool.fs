﻿[<AutoOpen>]
module FStap.DrawTool
open System open System.Windows open System.Windows.Controls

let lastToolChange = Stopwatch'()

let changeByDragOk<'__> = 
  lastToolChange.Elapsed > msec 500

let changeTool tool _ =
  if Layer.drawTool <>. tool then
    lastToolChange.restart
    Layer.drawTool <*- tool

let toolSelected tool (kpnl:KeyPanel) =
  Layer.drawTool => fun __ ->
    kpnl.pnl |> (If (__ = tool) "fee" "fff").bg 

// event
Layer.co' =+ { 
  if Layer.drawTool <>. DrawTool.Bucket then
    Layer.drawTool <*- DrawTool.Pen }
