﻿module FStap.Eraser
open System open System.Windows open System.Windows.Controls


let btnEraser =
  let __ = HStackPanel()
  let  _ = Ico.eraser $ __
  let  _ = RadOpaPanel (Layer.radEraser, Layer.opaEraser) $ __
  KeyPanel(
    __,
    "押す: 消しゴムツールを選択\r\n↑↓: 消しゴムの不透明度を変更\r\n←→: 消しゴムの太さを変更",
    pushf = changeTool DrawTool.Eraser , 
    dragf = fun v -> 
      if changeByDragOk then
        cursorModified.restart
        Layer.radEraser.[_1 + v.X / 2.   >> within 1. 100.]
        Layer.opaEraser.[_1 + v.Y / 100. >> within 0. 1. ] )

btnEraser |> toolSelected DrawTool.Eraser