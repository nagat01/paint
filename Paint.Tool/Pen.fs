﻿module FStap.Pen
open System open System.Windows open System.Windows.Controls

let btnPen =
  let __ = HStackPanel() $ vaStretch
  let  _ = Ico.pen $ __
  let  _ = BrushLabel(Layer.co', Layer.radPen, Layer.opaPen) $ __
  let  _ = BrushPanel(Layer.radPen, Layer.opaPen) $ __
  KeyPanel(
    __,
    "押す: ペンツールを選択\r\n↑↓: ペンの不透明度を変更\r\n←→: ペンの太さを変更",
    pushf = changeTool DrawTool.Pen,
    dragf = fun v -> 
      if changeByDragOk then
        cursorModified.restart
        Layer.radPen.[_1 + v.X / 2.   >> within 0.25 25.]
        Layer.opaPen.[_1 + v.Y / 100. >> within 0.   1. ]
    )
    
btnPen |> toolSelected DrawTool.Pen