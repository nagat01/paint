﻿module FStap.Select
open System open System.Windows open System.Windows.Controls  
open System.Windows.Input


let stabilizer = Stabilizer()

let rad = sh 25.

let image = Image() $ sz' S.sz'
let mutable paint = image.mkPaint S.sz


// events
S.state >< S.tool =+ { 
  image.IsHitTestVisible <- IsSelect 
  do! IsSelect.n 
  paint.undo }

S.sz' => fun sz -> 
  paint <- image.mkPaint sz

trCanvas >< rad >< loaded =+? {
  image.Cursor <- mkCursor 2 IsSpace DrawTool.Pen rad.v 1. (rad *. scCanvas) "c7f".co }

image.lmdown =+ {
  paint.wb.Lock() }

image.lDrag => fun e ->
  let r0, r1, p0, p1 = stabilizer.next e false
  let rf = rad /. 2.
  let co = density 0.5 "c7f".co
  let buffer = Layer.all.[1].paint.buffer.arr
  paint.draw IsShift buffer DrawMode.Marker co 1. (rf * r0) (rf * r1) p0 p1 

image.lmup =+ {
  paint.wb.Unlock() }

// buttons
let btnOver  = keyLbl (HStackPanel "別レイヤーに上書き") "押す: 選択範囲を別レイヤーに上書きします"
let btnWrite = keyLbl (HStackPanel "別レイヤーに書き込み") "押す: 選択範囲を別レイヤーに書き込みます"
let btnClear = keyLbl (HStackPanel "範囲選択クリア") "押す: 範囲選択をクリアします"

btnClear.down =+ { paint.undo }
btnOver .down =+ { paint.transfer true  Layer.cur.wb Layer.opp.wb }
btnWrite.down =+ { paint.transfer false Layer.cur.wb Layer.opp.wb }


// select button
let btnSelect =
  let panel = RadOpaPanel(rad, sh 1.)
  keyIco Ico.select panel "押す: 選択ツールを選択します。\r\n←→: 選択ツールの太さを変更"

btnSelect.down =+ { 
  S.tool <*- Tool.Select }

btnSelect.dragd => fun v ->
  rad +. v.X / 2. |> within 0. 100. |>* rad


// constant
let btns = [
  Key.W, btnClear
  Key.E, btnOver
  Key.R, btnWrite ]