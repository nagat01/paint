﻿[<AutoOpen>]
module FStap.Controls
open System open System.Windows open System.Windows.Controls
open System.Windows.Media
open System.Windows.Shapes


type BrushLabel(co:Color sh, rad:f sh, opa:f sh) as __ =
  inherit Canvas()
  let mutable _isEnabled = true
  let label() = Label'() $ fsz 8 $ "cfff".bg $ haLeft

  let  dpLbl   = DockPanel()    $ __ $ w 24 $ mgnw 4
  let   lblOpa = label()   $ dpLbl.bottom 
  let   lblRad = label()   $ dpLbl.bottom 
  let  dpElp   = DockPanel() $ __.xy 10 0 $ size1 25 $ zIdx -1
  let   elp    = Ellipse() $ dpElp $ w 1
  let Rad rad =
    sf "%.1f" rad |>* lblRad
    elp |> size1 rad
  let Opa opa =
    sf "%.2f" opa |>* lblOpa
  do
    __ |> w 37.5  
    co >< opa =+ { 
      if __.isEnabled then 
        fill (density opa.v co.v) elp }
    rad => Rad
    opa => Opa
    Rad 3.
    Opa 1.

  member __.isEnabled 
    with get() = _isEnabled 
    and  set v = _isEnabled <- v


type BrushPanel(rad:f sh, opa:f sh) as __ =
  inherit HStackPanel()   
  do __ $ h 26 |> vaCenter
  let  cnv           = Canvas()  $ __  $ tr2 -12.5 0 $ haRight
  let   tri          = Polygon() $ cnv $ fill "ddd" $ xys3 0. 12.5 50. 0. 50. 25.
  let   lnHorizontal = Line()    $ cnv $ stroke 0.5 "000"
  let   lnVertical   = Line()    $ cnv $ stroke 0.5 "000"
  do
    rad >< opa =+ {
      let x = rad *. 2.
      let y = opa *. 25.
      lnVertical   |> xy2 x 0 x 25
      lnHorizontal |> xy2 0 y 50 y }


type RadOpaPanel(rad:f sh, opa:f sh) as __ =
  inherit Grid()
  do __ $ h 26 $ tr2 -26. 0. $ vaCenter |> ig
  let cnv      = Canvas()     $ __  $ wh 100 26 $ haRight $ clipToBounds
  let  sp      = StackPanel() $ __  $ mgnw 4 $ vaCenter $ haRight
  let   lblRad = Label'()  $ sp  $ fsz 10 $ w 24 
  let   lblOpa = Label'()  $ sp  $ fsz 10 $ w 24 
  let  elp     = Ellipse() $ cnv $ stroke 0.5 "000" $ fill "fff" $ zIdx 1 $ center
  let  lnv     = Line()    $ cnv $ stroke 0.5 "700"
  let  lnh     = Line()    $ cnv $ stroke 0.5 "007"
  do
    rad >< opa =+ {
      let rad, opa = rad.v, opa.v
      let x    = rad
      let y    = opa * 25.
      let half = rad / 2.
      sf "%.2f" opa |>* lblOpa
      sf "%.1f" rad |>* lblRad
      elp $ mv 0 (13. - half) |> size1 x
      lnv |> xy2 x 0. x 26.
      lnh |> xy2 0 y 100 y
    }
