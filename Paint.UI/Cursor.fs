﻿[<AutoOpen>]
module FStap.Cursor
open System open System.Windows open System.Windows.Controls

// cursor
open System.IO
open System.Globalization
open System.Windows.Interop
open System.Windows.Media
open System.Windows.Media.Imaging
open System.Runtime.InteropServices
open Microsoft.Win32.SafeHandles

let inline formattedText s sz (co:s) =
  FormattedText(
    s, 
    CultureInfo.CurrentCulture, 
    FlowDirection.LeftToRight,
    Typeface( FontFamily "Arial", FontStyles.Normal, FontWeights.Normal, FontStretch() ),
    !.sz,
    co.br)

type DrawingContext with
  member __.drawText s sz co x y =
    let rad = formattedText s sz co
    __.DrawText(rad, po x y)

let rtbToBitmap (rtb:RenderTargetBitmap) =
  let encoder = PngBitmapEncoder()
  encoder.Frames <+ BitmapFrame.Create rtb 
  use ms = new MemoryStream()
  encoder.Save ms
  new Drawing.Bitmap(ms)


let mkCursor idx isShowProp (dt:DrawTool) rad opa (r:f) (co:Color) =
  let r    = If (dt = DrawTool.Bucket) 0. r
  let r'   = (r / 1.6).i
  let half = max 16 r.i
  let w    = half * 2
  let yo   = 40
  let xo   = 20
  let rtb = mkRtb (sz (w + xo * 2) (w + yo * 2))
  if isShowProp then
    rtb.render <| fun dc ->
      let x   = xo + half - 25
      let y   = yo + half + max 16 r' + 4
      let rad = sf "%.1f" rad
      let opa = sf "%.0f" (opa * 100.)
      match dt with
      | DrawTool.Pen ->
        dc.DrawRectangle("fff".br, Pen(), Rect(!.x, !.y, 50., 30.))
        dc.drawText co.r.s 10 "f00" x y
        dc.drawText co.g.s 10 "0f0" x (y+10)
        dc.drawText co.b.s 10 "00f" x (y+20)
        dc.drawText rad    10 "000" (x + 25) y
        dc.drawText opa    10 "000" (x + 25) (y+10)
      | _ ->
        dc.DrawRectangle("fff".br, Pen(), Rect(!.(x + 20), !.y, 25., 30.))
        dc.drawText rad    10 "000" (x + 25) y
        dc.drawText opa    10 "000" (x + 25) (y+10)

  use bmp = rtbToBitmap rtb
  for y in yo .. w-1 + yo do
    for x in xo .. w-1 + xo do
      let y' = y - half - yo
      let x' = x - half - xo
      let isInside r = sqr x' + sqr y' < r * r
      let coDotted = (x + y).isEven.If 0xcc000000 0xccffffff
      let set co = bmp.SetPixel(x, y, Drawing.Color.FromArgb co)
      if (x' = 0 || y' = 0) && isInside (max 16 r') then
        set coDotted
      elif isInside r' then 
        set co.i
      elif isInside (r' + 1) then
        set coDotted
  bmpToCursor idx bmp
