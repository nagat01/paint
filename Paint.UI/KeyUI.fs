[<AutoOpen>]
module FStap.KeyUI
open System open System.Windows open System.Windows.Controls
open System.Windows.Input


// funcs
let rightAngleVector __ =
  __ |> Observable.scan (fun (sx, sy, _) (__:Vec) ->
    let x  = __.X
    let y  = __.Y
    let sx = x + sx * 0.8 
    let sy = y + sy * 0.8
    let ax = abs sx
    let ay = abs sy
    let vx = If (ax > ay * 0.5) x 0.
    let vy = If (ay > ax * 0.5) y 0.
    sx, sy, vec vx vy
    ) (0., 0., vec 0 0)
  |%> __x


// keyboard
let mkKeyboardPanel<'__> = StackPanel() $ "cedf".bg $ mgn1 1 $ haLeft

let line0 = [ Key.Escape; Key.D1; Key.D2; Key.D3; Key.D4; Key.D5 ]
let line1 = [ Key.Tab;      Key.Q;  Key.W;  Key.E;  Key.R;  Key.T ]
let line2 = [ Key.LeftCtrl;   Key.A;  Key.S;  Key.D;  Key.F;  Key.G ]
let line3 = [ Key.LeftShift;    Key.Z;  Key.X;  Key.C;  Key.V;  Key.B ]

let lines = [ line0; line1; line2; line3 ]

let grids = Dic()

let kuiTop    = mkKeyboardPanel 
let kuiBottom = mkKeyboardPanel


do' {
for y, line in lines.seqi do
  let sp = HStackPanel() $ h 30 $ (y < 3).If kuiTop kuiBottom
  for x, key in line.seqi do
    let grid = Grid() $ sp $ w (If (x = 0) (30 + y * 10) 100)
    grids.Add (key, grid)
}


// key button
type KeyPanel(pnl:Panel, _tip:s, ?pushf, ?dragf, ?_w) as __ =
  inherit Grid()
  let _key = None'<Key>
  let check checkKey = __.isTop && _key.ok && checkKey _key.v && TextBoxFocused.n
  let isKey (e:KeyEventArgs) = check ((=)e.Key)  
  let isKeyDown _ = check isKeyDown 
  let pku = mw.pkup -?? isKey
  let pkd = mw.pkdown -?? isKey

  let lblKey = ShadowedLabel(10., "aaf") $ vaTop $ tr (po 0 -2) $ zIdx 1 
  do
    __ $ "fff".bg |> cs [lblKey; pnl]
    __ |> tip' (fun _ -> __.guide)

  member val enter = 
    let e = __.menter
    e =+ { "dfd".bg __ }
    e
 
  member val up    = 
    let e = __.mleave >< pku
    e =+ { "fff".bg __ }
    e

  member val down  = 
    let e = __.lmdown >< pkd 
    e =+ { 
      "fdd".bg __
      guide <*- __.guide 
      (pushf |? ig)() }
    e    

  member val dragd = 
    let e = mw.drag -?? isKeyDown |> rightAngleVector 
    e => (dragf |? ig)
    e
  
  member __.Key key = 
    _key   <*- key
    lblKey <*- keyToS key

  member __.pnl  = pnl

  member __.grid_ = __.parent :?> Grid

  member __.isTop = __.grid_.ok && (__.grid_.cs.last :> obj) = (__ :> obj)

  member __.show key =
    grids.find key |%| fun grid -> 
      __.Key key
      grid.rem __
      grid.add __

  member __.hide = 
    try' { __ |> __.grid_.rem }

  member __.swap key = 
    if __.isTop then __.hide else __.show key

  member __.guide =
    _key |%| fun key -> 
      guide |> If (line3.have key) vaBottom vaTop
    _tip.lines 
    |%> ((_key |%> keyToS |? "") + " + ") + _1
    |> String.concat "\r\n"


// ctors
let icoBtn ico ttl =
  HStackPanel() $ cs [ico;Lbl ttl $ mgnw 2] $ haStretch

let keyLbl        ctt tip   = KeyPanel (ctt, tip)
let keyBtn        ctt tip f = KeyPanel (ctt, tip, pushf=f)
let keyDragr      ctt tip f = KeyPanel (ctt, tip, dragf=f)
let keyIco    ico ttl tip   = KeyPanel (icoBtn ico ttl, tip)
let keyIcoBtn ico ttl tip f = KeyPanel (icoBtn ico ttl, tip, pushf = f)


// funcs
let show      (__:seq<Key*KeyPanel>) = for key, kpnl in __ do kpnl.show key
let hide      (__:seq<Key*KeyPanel>) = for _,   kpnl in __ do kpnl.hide
let vis isVis (__:seq<Key*KeyPanel>) = If isVis show hide __
let swap      (__:seq<Key*KeyPanel>) = vis (__.forall(_x >> fun __ -> __.isTop).n) __
