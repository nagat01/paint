[<AutoOpen>]
module FStap.Layer
open FStap open System open System.Windows open System.Windows.Controls  

let stabilizer = Stabilizer()


// layer
let cnvLayer = Canvas() $ "fff".bg $ sz' S.sz'

type Layer (idx:i) as __ =
  inherit Image()
  let mutable _drawTool = DrawToolInfo idx
  let mutable _paint    = __.mkPaint S.sz
  let lastDown          = Stopwatch'()
  let draw isShift (rad0, rad1, p0, p1) =
    let getRad rad = rad * _drawTool.rad / 2.
    let isLeak = idx = 1 || IsShift || ( _drawTool.radPen.v < 5. )
    let buffer = Layer.all.[1].paint.buffer.arr
    let draw drawTool (__:Paint) = 
      __.draw 
        isLeak buffer
        drawTool _drawTool.co _drawTool.opa 
        (getRad rad0) (getRad rad1) p0 p1
    match _drawTool.drawTool.v with 
    | Pen    -> draw DrawMode.Pen  _paint
    | Blur   -> draw DrawMode.Blur _paint
    | Eraser -> 
      let mode = IsCtrl.If DrawMode.EraserSubtract DrawMode.Eraser
      if isShift then
        for __ in Layer.all do
          draw mode __.paint
      else
        draw mode __.paint
    | _      -> () 
     
  do
    scHigh __
    S.sz' => __.Sz

    let mutable latest = 0
    _drawTool >< Layer.idx >< trCanvas >< loaded =+! {

    if __.isSeld then
      let isShowProp = cursorModified.msec < 10.
      let mkCursor isShowProp = 
        let dt  = _drawTool
        let rad = dt.rad * scCanvas 
        let co  = ( dt.drawTool =. DrawTool.Pen ).If dt.coPen "ccc".co
        try' { __.Cursor <- mkCursor idx isShowProp dt.drawTool.v dt.rad dt.opa rad co } 
      mkCursor isShowProp
      let temp = &latest +=! 1
      do! isShowProp
      do! 1000
      if latest = temp then
        mkCursor false }

    __.StylusDown =+ { 
      lastDown.restart 
      }

    __.StylusUp =+ {
      do! lastDown.IsRunning 
      let p   = MPosOn __
      let rad = lastDown.sec * 4. |> within 0. 1.
      draw IsShift (rad, rad, p, p)
      }

    __.lmdown =+ { 
      stabilizer.clear
      }

    __.lDragGap 100. => fun e ->
      lastDown.stop
      stabilizer.next e (Layer.idx =. 1 && _drawTool.drawTool =. DrawTool.Pen && _drawTool.radPen.v < 5. )
      |> draw IsShift

    wnd.lmup =+ {
      if S.state =. State.DrawTool then
        if _drawTool.drawTool =. DrawTool.Bucket then
          __.bucket _drawTool.mgnBucket.v
        _paint.setBuffer
      }

  member __.drawTool = _drawTool
  member __.wb
    with get () : Wb = __.Source :?> Wb 
    and  set (wb:Wb) = _paint <- __.mkPaint wb
  member __.paint:Paint = _paint 

  member __.isSeld = Layer.idx =. idx 
  member val opa = 
    let opa = sh 1. 
    __.set_Opacity <-* opa
    opa

  member __.ini =
    __.wb <- mkWriteableBitmap S.iw S.ih
    _drawTool.ini

  member __.bucket margin = 
    if __.IsMouseOver then
      let wbs = [| for __ in Layer.all -> __.wb |]
      __.paint.bucket wbs _drawTool.co margin __.mpos

  static member val idx : i sh    = sh 0
  static member val all : Layer[] = Array.init 2 (fun i -> Layer i $ cnvLayer)


// members
let cur<'__> = Layer.all.[Layer.idx.v]
let opp<'__> = Layer.all.[1 - Layer.idx.v]
let manips   = seq { for __ in Layer.all -> __.paint }

// obs
let mkShs f = 
  let __ = shs [ for layer in Layer.all -> f layer.drawTool ]
  Layer.idx => __.Idx
  __.sh


// properties
let hsv<'__>   = cur.drawTool.hsv
let co<'__>    = cur.drawTool.co
let cos<'__>   = cur.drawTool.cos
let coPen<'__> = cur.drawTool.coPen
let Co co      = cur.drawTool.Co co


// shared values
let co'        = mkShs <| fun tool -> tool.co'
let idxCo      = mkShs <| fun tool -> tool.idxCo
let drawTool   = mkShs <| fun tool -> tool.drawTool
let opaPen     = mkShs <| fun tool -> tool.opaPen
let radPen     = mkShs <| fun tool -> tool.radPen
let radEraser  = mkShs <| fun tool -> tool.radEraser
let opaEraser  = mkShs <| fun tool -> tool.opaEraser
let radBlur    = mkShs <| fun tool -> tool.radBlur
let opaBlur    = mkShs <| fun tool -> tool.opaBlur
let mgnBucket  = mkShs <| fun tool -> tool.mgnBucket
let diffBucket = mkShs <| fun tool -> tool.diffBucket


// funcs
let ini<'__>  = for __ in Layer.all do __.ini

let spoit<'__> =
  if cur.IsMouseOver 
  then cur.wb.pixel cur.mpos
  else getColorOnScreen MousePos 
  |>* cur.drawTool.co'

let initCanvasPos<'__> =
  S.angle <*-* S.defaultAngle
  min (gridCanvas.rw /. S.w) (gridCanvas.rh /. S.h) 
  |> trCanvas.Scale
  po -((1. - trCanvas.scX) / 2. *. S.w) 0.
  |> trCanvas.Move
  S.angle 
  |*> trCanvas.Angle

// paint
let setBuffer<'__>      = for __ in manips do __.setBuffer 
let restoreBuffer<'__>  = for __ in manips do __.restoreBuffer
let deform b os ps cs   = for __ in manips do __.deform b os ps cs
let flipHorizontal<'__> = for __ in manips do __.flipHorizontal
let flipVertical<'__>   = for __ in manips do __.flipVertical

let rotateRightAngle isClockwise =
  for __ in manips do __.rotate isClockwise
  let w = S.w.v
  let h = S.h.v
  S.w <*- w
  S.h <*- h


// event
Layer.idx =+ { 
  for __ in Layer.all do
    __.IsHitTestVisible <- __.isSeld }

