[<AutoOpen>]
module FStap.Main
open System open System.Windows open System.Windows.Controls

let Ico = ImageProvider 25.

let wnd = Window' "SakuSakuPaint" $ wh 600 600 $ "ddd".bg $ restorePosSz


// model
module S =
  let w       = pshf "w" 960.
  let h       = pshf "h" 1200.
  let sz'     = ( w >< h ) -% { yield sz w.v h.v }
  let iw<'__> = w.v.i
  let ih<'__> = h.v.i
  let sz<'__> = sz w.v h.v
  let file    = pshs "file" ""
  let tool    = sh Tool.Select
  let state   = sh State.DrawTool

  let defaultAngle = pshf "defaultAngle" 0.
  let angle = sh defaultAngle.v

  file => fun file ->
    wnd.Title <- file.Or "SakuSakuPaint"

  state => function
    | Tool -> tool.ini
    | _    -> ()

let mutable cursorModified = Stopwatch'()

// view
let dpWindow    = DockPanel()   $ wnd $ lastChildFill
let  gridCanvas = Grid()   
let  lblConsole = Label'() $ "cfff".bg $ aTopLeft
let   _         = guide    $ "cfff".bg $ aTopRight 
let   cnvCanvas = Canvas() $ "fff".bg  $ aCenterLeft 
let    trCanvas = Transformer (cnvCanvas, SRT)


// funcs
let toggleState state =
  S.state <*- If (S.state =. state) State.DrawTool state

let isTool tool = 
  S.state =. State.Tool && 
  S.tool  =. tool

let IsDeform<'__> = isTool Tool.DeformUI
let IsSelect<'__> = isTool Tool.Select

let scCanvas<'__> = trCanvas.sc' * (cnvCanvas.w /. S.w)


// events
S.angle => trCanvas.Angle

S.sz' => cnvCanvas.Sz

lblConsole.mdown =+ { lblConsole <*- "" }

let ini = wnd.ig