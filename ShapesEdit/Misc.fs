﻿[<AutoOpen>]
module Misc

open System open System.Windows open System.Windows.Controls

open System.Windows.Input
open System.Windows.Media
open System.Windows.Shapes

//
// shapes edit
//
[<Qual>]
type SeTools = Line | Polyline | Bezier

module S =

  open System.Windows.Media
  open System.Windows.Shapes

  let mutable curTool = SeTools.Bezier

  let wnd = GlassWindow "ShapesEdit"

  let ca = Ca () $ wh 400 400 $ "fff".bg

  let mutable cpsVis = true
  let cps = rarray<Ellipse>()


let stroke (__:Shape) = 
  __.Stroke <- Brushes.Black
  __.StrokeThickness <- 1.

let isInside (p:Po) = isInside2 S.ca.w S.ca.h p
  
let triggerMouseLDown (uie:UIE) =
  uie.RaiseEvent(
    MouseButtonEventArgs(
      Mouse.PrimaryDevice, 
      Environment.TickCount, 
      MouseButton.Left, 
      RoutedEvent = UIElement.PreviewMouseDownEvent))
  
let mkCp (ca:Ca) (co:Co) (p:Po sh) =
  let cp = 
    Ellipse() $ ca $ S.cps 
    $ p.v.mv $ tr1 -4 $ zIdx 1
    $ size1 8 $ fill co $ 0.3.opa   

  p => fun p -> p.mv cp
  cp.rDragAbs => fun e -> p <*- e.Cur
  cp

let mkCp' (p:Po sh) = mkCp S.ca "0f0".co p