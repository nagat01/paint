open System open System.Windows open System.Windows.Controls open System.Reflection
[<product "ShapesEdit";  version  "0.0.0.0"; company "ながと"; copyright"Copyright © ながと 2014";
  title "ShapesEdit: System.Windows.Shapesで描画する図形エディタ"; STAThread>] SET_ENG_CULTURE

let toggleCpsVis () =
  notm &S.cpsVis
  for cp in S.cps do cp.Vis S.cpsVis

let btnToggleCpsVis = 
  KbMembers.Btn("制御点", toggleCpsVis)

/// 画面全体
let dp = Dpnl () $ S.wnd
let  gkbif = 
 mkGraphicalKbIf S.wnd [
  Key.D, SeTools_Polyline.btn  
  Key.S, SeTools_Line.btn       
  Key.F, SeTools_Bezier.btn

  Key.V, btnToggleCpsVis
 ] 
 $ dp.top

/// キャンバス
let _ = S.ca $ dp.top

SeTools_Bezier.bezierCurveAppender.Path |> S.ca

S.wnd.run 