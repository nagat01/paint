﻿module SeTools_Bezier

open System open System.Windows open System.Windows.Controls  

open System.Windows.Input
open System.Windows.Media
open System.Windows.Shapes

let btn = KbMembers.Btn ("Bezier", fun _ -> S.curTool <- SeTools.Bezier)

let Enabled _ = S.curTool = SeTools.Bezier

type IVisibleSettable = abstract isVis:b -> u

let mutable lastCp : Ellipse opt = None
type EditablePoint(p0:Po sh) = 
  let cp = mkCp S.ca "f00".co p0 
  do 
    lastCp.[ fun __ -> __.Fill <- Brushes.Blue ]
    lastCp <- Some cp
  interface IVisibleSettable with 
    member __.isVis isVis = cp.Vis isVis

type EditableLever(p0:Po sh, p1:Po sh) =
  let cp = mkCp' p1
  let line = Line(Stroke=Brushes.Green) $ p0p1 p0.v p1.v $ 0.3.opa $ S.ca
  do 
    p0 => line.set_p1 
    p1 => line.set_p2 
  interface IVisibleSettable with 
    member __.isVis isVis = isVis $ cp.Vis |> line.Vis 

type BezierCurveAppender (newBezier : kea obs) =
  let mutable phase = 0
  let mutable curPathFigure = PathFigure()
  let pathGeometry = PathGeometry [curPathFigure]
  let path = Path(Data=pathGeometry, Stroke=Brushes.Black)
  let mutable setPointHandler = Def
  let mutable lastP3 = sh Po.None
  let mutable p0, p1, p2, p3 = Def, Def, Def, Def
  
  let mkBezierCurve (p0:Po sh) p =
    p1 <- sh Po.None
    p2 <- sh Po.None
    p3 <- sh Po.None
    lastP3 <- p3
    
    let setPoint p =   
      [|p3; p1; p2|] |%| fun __ -> __ <*- p
      EditablePoint(p3).ig
      EditableLever(p0, p1).ig
      EditableLever(p3, p2).ig

    setPointHandler <- setPoint

    let bezierSegment = BezierSegment () $ curPathFigure.Segments.Add
    p1 => bezierSegment.set_Point1 
    p2 => bezierSegment.set_Point2 
    p3 => bezierSegment.set_Point3 

    setPoint p

  do (S.ca.lmup >=< S.ca.lmdown).[fun e -> e.Source = box S.ca && Enabled ()] => fun e ->
    let p = e.pos
    match phase with
    | 0 ->
      p0 <- sh Po.None
      lastP3 <- p0
      lastP3 => curPathFigure.set_StartPoint
      lastP3 <*- p
      EditablePoint lastP3 |> ignore
    | 1 -> phase <- 3; mkBezierCurve lastP3 p
    | _ -> if phase <= 3 then setPointHandler p
    phase <- phase % 3 + 1

  let mutable prev = Po.None
  do S.ca.MouseMove => fun e ->
    let vec = e.pos - prev
    prev <- e.pos
    try' {
    let (|Down|_|) key () = Keyboard.IsKeyDown(key).some () 
    if vec.X <> Double.NaN then
      match () with
      | Down Key.Q -> p0 <*- p0.v + vec 
      | Down Key.W -> p1 <*- p1.v + vec 
      | Down Key.E -> p2 <*- p2.v + vec 
      | Down Key.R -> p3 <*- p3.v + vec
      | _ -> () }

  do newBezier.[Enabled] =+ {
    curPathFigure <- PathFigure ()
    pathGeometry.Figures.Add curPathFigure
    phase <- 0 }
                                   
  member __.Path = path

let bezierCurveAppender = BezierCurveAppender(S.wnd.kdown Key.A)
