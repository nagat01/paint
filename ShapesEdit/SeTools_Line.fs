﻿module SeTools_Line

open System open System.Windows open System.Windows.Controls

open System.Windows.Media
open System.Windows.Shapes

let btn = KbMembers.Btn ("Line", fun _ -> S.curTool <- SeTools.Line)
/// tempLine 操作中の直線
let tl = Line () $ stroke $ collapsed $ S.ca

let enabled<'__> = S.curTool = SeTools.Line

let mutable orig = Po.None
S.ca.lmdown => fun e ->
  orig <- e.pos
  if enabled then
    tl |> vis

S.ca.lDrag => fun e ->
  if S.ca.isInside e.Prev || S.ca.isInside e.Cur then
    if enabled then p0p1 e.Orig e.Cur tl

S.ca.lmup => fun e ->
  if enabled && orig <> Po.None then
    let p1, p2 = sh orig, sh e.pos
    let line = Line() $ stroke $ p0p1 p1.v p2.v $ S.ca
    p1 => line.set_p1
    p2 => line.set_p2
    p1 |>~ mkCp'
    p2 |>~ mkCp'
    tl |> collapsed