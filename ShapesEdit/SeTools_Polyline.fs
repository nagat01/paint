﻿module SeTools_Polyline
open System open System.Windows open System.Windows.Controls
open System.Windows.Media
open System.Windows.Shapes

let btn = KbMembers.Btn ("Polyline", fun _ -> S.curTool <- SeTools.Polyline)

/// tempPolyline 操作中の直線
let mutable tpl = Def<Polyline>

// controlPoints 制御点
let cps : Ellipse rarray = rarray ()

// addPolylineControlPoint 制御点の追加
let addCp p =
  tpl.Points.Add p
  let p = sh p
  let _ = mkCp' p $ cps  
  p => 
    let idx = tpl.Points.len - 1
    fun __ -> tpl.Points.[idx] <- __

let enabled _ = S.curTool = SeTools.Polyline  

// polylineで描き始める
S.ca.lmdown.[enabled] => fun e ->
  let polyline = Polyline() $ stroke $ S.ca
  tpl <- polyline
  addCp e.pos

// polylineを描いていく
let mutable prevCp = Po.Outside
S.ca.lDrag.[enabled] => fun e ->
  if (isInside e.Prev || isInside e.Cur) && e.Cur.dist prevCp > 10. then
    prevCp <- e.Cur
    addCp e.Cur

// polylineの描き終わり
S.wnd.lmup.[enabled] =+ { tpl.Points <+ S.ca.mpos }